using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Menu, Playing, Pause, Settings, Result, Gameover, BossFight, ArcanoidShop, TunnelFinalResult
}
public class GameController : MonoBehaviour
{
    private static GameController _instance; //Что то типа singleton'а
    
    [SerializeField] private SettingsMenu settingsMenu;

    public static EventValue<GameState> State { get => _instance._state; set => _instance._state= value; } //Текущая стадия игры
    private EventValue<GameState> _state;

    private int _maxLives=2;
    private int _scoreRecord=0;
    
    private void Start(){
        
        if(_instance!=null) Destroy(_instance.gameObject);
        _instance = this;
        
        Application.targetFrameRate = 60;
        //settingsMenu.LoadSettingsData();
        _state = new EventValue<GameState>(GameState.Menu) {Value = GameState.Menu};
        InterfaceManager.Toggle(MenuName.Main);
        _state.onChanged += OnStateChanged;
    }

    private void OnStateChanged(GameState newValue,GameState oldValue)
    {
        switch (newValue)
        {
            case GameState.Menu:
                Time.timeScale = 1;
                InterfaceManager.Toggle(MenuName.Main);
                break;
            
            case GameState.Gameover:
                Time.timeScale = 0;
                InterfaceManager.Toggle(MenuName.Gameover);
                InterfaceManager.SetRecord(_scoreRecord);
                break;
            
            case GameState.Pause:
                Time.timeScale = 0;
                InterfaceManager.Toggle(MenuName.Pause);
                break;

            case GameState.Settings:
                Time.timeScale = 0;
                InterfaceManager.Toggle(MenuName.Settings);
                break;

            case GameState.Result:
                Time.timeScale = 0;
                InterfaceManager.Toggle(MenuName.Result);
                break;

            case GameState.Playing:
                Time.timeScale = 1;
                InterfaceManager.Toggle(MenuName.Game);
                break;

            case GameState.ArcanoidShop:
                Time.timeScale = 0;
                InterfaceManager.Toggle(MenuName.ArcanoidShop);
                break;

            case GameState.TunnelFinalResult:
                Time.timeScale = 0;
                Debug.Log("Tunnel Boss defeated!");
                InterfaceManager.Toggle(MenuName.Result);
                break;

        }
    }
}
