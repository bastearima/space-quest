﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Serialization;

public class PlayerController : MonoBehaviour
{
    // Блок персонажа, устанавливаем скорость, переменную движения, сам риг и количество хп
    [SerializeField] private float _maxSpeed, _usualSpeed, _currentSpeed;

    [SerializeField] private float _moveX;

    private Rigidbody2D _rb;

    [SerializeField] private int _maxHealth, _currentHealth;

    private bool _faceRight = true;

    [SerializeField] private GameObject _respawn;

    //Переменные для определения "прибитости" персонажа к земле
    [SerializeField] private float _jumpPower;
    private bool _isGrounded;
    [SerializeField] private Transform _feetPos;
    [SerializeField] private float _checkRadius;
    [SerializeField] private LayerMask _whatIsGround;

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        _moveX = Input.GetAxis("Horizontal");
        Move(_moveX);

        if (Input.GetKey(KeyCode.UpArrow))
        {
            Jump();
        }
    }

    private void Jump()
    {
        if (_isGrounded)
        {
            _rb.AddForce(Vector2.up * _jumpPower, ForceMode2D.Impulse);
        }
    }

    private void Move(float horizonalInput)
    {
        _rb.velocity = new Vector2(horizonalInput * _currentSpeed, _rb.velocity.y);
        if (horizonalInput > 0 && !_faceRight)
            Flip();
        else if (horizonalInput < 0 && _faceRight)
            Flip();
    }

    private void Flip()
    {
        _faceRight = !_faceRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
            _isGrounded = true;
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Platform")
            this.transform.parent = null;
        if (col.gameObject.tag == "Ground")
            _isGrounded = false;
    }

    private void OnCollisionStay2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Platform")
            _rb.transform.parent = coll.transform;
    }
}