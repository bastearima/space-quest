﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationSystem : MonoBehaviour
{
    private LocalizationObject[] localizationObjects;
    [SerializeField] private LocalizationData localizationData;

    public LocalizationData LocalizData => localizationData;

    private void Start()
    {
        LocalizeEverything();
    }

    private void FindLocalizationObjects()
    {
        localizationObjects = GameObject.FindObjectsOfType<LocalizationObject>(true);
    }
    private void InitialLocalizationObjects()
    {
        LocalizationData locData = localizationData;
        foreach (var localizationObject in localizationObjects)
        {
            if (locData.itemsDict.ContainsKey(localizationObject.Key))
            {
                localizationObject.Init(locData.itemsDict[localizationObject.Key].dict[locData.lang]);
            }
            else
            {
                Debug.LogError($"Key - {localizationObject.Key} doesnt exist in data");
            }
        }
    }

    public void LocalizeEverything()
    {
        FindLocalizationObjects();
        InitialLocalizationObjects();
    }
}
