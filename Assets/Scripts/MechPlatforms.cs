﻿using UnityEngine;

public class MechPlatforms : MonoBehaviour
{
    public GameObject mechanism;
    // Выбор направления движения платформы
    // Up - Down = true, Left - Right = false
    public bool UpOrLeft = false;
    public float MaxPostion = 0f;
    public float MinPostion = 0f;
    // Определяет ли этот объект включения портала на следующий уровень
    public bool NeedPortalOn;
    // Выбираем портал, если выбрали, что он активируется
    public GameObject finishPortal;
    // Скорость платформы
    public float moveSpeed = 6f;
    bool moveBack = true;
    // Включить ли механизм (всегда выбираем, если хотим изначальную работу - сразу в объекте ставим галку)
    public bool startMech;
    float FinalDirection;
    public Sprite sprite;

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            this.GetComponent<SpriteRenderer>().sprite = sprite;
            startMech = true;
            if (NeedPortalOn)
                finishPortal.SetActive(true);
        }
    }

    void Update()
    {
        if (startMech == true)
        {

            if (UpOrLeft)
                FinalDirection = mechanism.transform.position.y;
            else
                FinalDirection = mechanism.transform.position.x;

            if (FinalDirection > MaxPostion)
                moveBack = false;
            if (FinalDirection < MinPostion)
                moveBack = true;

            float op1 = 0;
            float op2 = 0;

            if (UpOrLeft)
                op2 = moveSpeed * Time.deltaTime;
            else
                op1 = moveSpeed * Time.deltaTime;

            if (moveBack)
                mechanism.transform.position = new Vector2(mechanism.transform.position.x + op1, mechanism.transform.position.y + op2);
            else
                mechanism.transform.position = new Vector2(mechanism.transform.position.x - op1, mechanism.transform.position.y - op2);
        }
    }

}
