﻿using System;
using UnityEngine;
using Firebase.Database;
using Firebase.Auth;
using TMPro;

public class DatabaseSystem : MonoBehaviour
{
    public DatabaseReference databaseReference;
    [SerializeField] private UserData userData;

    public TextMeshProUGUI text;

    private void Awake()
    {
        databaseReference = FirebaseDatabase.DefaultInstance.RootReference;
    }

    private void Start()
    {
        FirebaseDatabase.DefaultInstance.GetReference("users").Child(FirebaseAuth.DefaultInstance.CurrentUser.UserId.ToString()).ValueChanged += HandleValueChanged;
    }

    public void LoadFromCloud()
    {
        FirebaseDatabase.DefaultInstance
      .GetReference("users").Child(FirebaseAuth.DefaultInstance.CurrentUser.UserId.ToString())
      .GetValueAsync().ContinueWith(task => {
          if (task.IsFaulted)
          {
              Debug.LogError("Load error");
          }
          else if (task.IsCompleted)
          {
              DataSnapshot snapshot = task.Result;
              UserInfo extractedData = JsonUtility.FromJson<UserInfo>(snapshot.GetRawJsonValue());
              userData.userName = extractedData.Username;
              userData.health.maxHealth = extractedData.MaxHealth;
              userData.balance.countOfGems = extractedData.CountOfGems;
              userData.balance.countOfCoins = extractedData.CountOfCoins;
              userData.score.ratingScore = extractedData.RatingScore;
          }
      });
        text.text = userData.score.ratingScore.ToString();
    }

    private void HandleValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        string playerData = args.Snapshot.GetRawJsonValue();
        UserInfo extractedData = JsonUtility.FromJson<UserInfo>(playerData);
        userData.userName = extractedData.Username;
        userData.health.maxHealth = extractedData.MaxHealth;
        userData.balance.countOfGems = extractedData.CountOfGems;
        userData.balance.countOfCoins = extractedData.CountOfCoins;
        userData.score.ratingScore = extractedData.RatingScore;
    }

    public void ClearAllPlayerFields()
    {
        UserInfo user = new UserInfo(FirebaseAuth.DefaultInstance.CurrentUser.DisplayName,
        0, 0, 0, 1);
        string json = JsonUtility.ToJson(user);
        databaseReference.Child("users").Child(FirebaseAuth.DefaultInstance.CurrentUser.UserId.ToString()).SetRawJsonValueAsync(json);
        LoadFromCloud();
    }

    public void SaveToCloud()
    {
        UserInfo user = new UserInfo(FirebaseAuth.DefaultInstance.CurrentUser.DisplayName,
        userData.score.ratingScore, userData.balance.countOfCoins, userData.balance.countOfGems, userData.health.maxHealth);

        string json = JsonUtility.ToJson(user);

        databaseReference.Child("users").Child(FirebaseAuth.DefaultInstance.CurrentUser.UserId.ToString()).SetRawJsonValueAsync(json);
    }

    public void GetUserValue(string userId, string field)
    {
        databaseReference.Child("users").Child(userId).Child(field).GetValueAsync();
    }

    public void RemoveUserValue(string userId, string field)
    {
        databaseReference.Child("users").Child(userId).Child(field).RemoveValueAsync();
    }

    public void SetUserValue(string userId, string field, string value)
    {
        databaseReference.Child("users").Child(userId).Child(field).SetValueAsync(value);
    }
}

