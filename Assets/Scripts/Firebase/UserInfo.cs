﻿using System;

[Serializable]
public class UserInfo
{
    public string Username;
    public int RatingScore;
    public int CountOfCoins;
    public int CountOfGems;
    public int MaxHealth;


    public UserInfo()
    {

    }

    public UserInfo(string name, int rating, int coins, int gems, int maxHealth)
    {
        Username = name;
        RatingScore = rating;
        CountOfCoins = coins;
        CountOfGems = gems;
        MaxHealth = maxHealth;
    }
}
