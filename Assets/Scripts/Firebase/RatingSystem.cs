﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;
using System.Linq;

public class RatingSystem : MonoBehaviour
{
    [SerializeField] private Text[] RatingTexts;

    public void LoadLeaderboard()
    {
        int topNum = 0;
        List<string> TopRating = new List<string>();
        FirebaseDatabase.DefaultInstance.GetReference("users").OrderByChild("RatingScore").LimitToLast(10).ValueChanged += (object sender2, ValueChangedEventArgs e2) =>
        {
            if (e2.DatabaseError != null)
            {
                Debug.LogError(e2.DatabaseError.Message);
                return;
            }

            foreach (var childSnapshot in e2.Snapshot.Children)
            {
                TopRating.Add(childSnapshot.Child("Username").Value.ToString() + " " + int.Parse(childSnapshot.Child("RatingScore").Value.ToString()));
            }
            TopRating.Reverse();
            foreach (var childSnapshot in e2.Snapshot.Children)
            {
                RatingTexts[topNum].text = TopRating[topNum];
                topNum++;
            }
        };
    }
}
