﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Auth;
using TMPro;

public class AuthenticationSystem : MonoBehaviour
{
    [Header("Firebase")]
    //public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;    
    public FirebaseUser User;

    //Login variables
    [Header("Login")]
    public TMP_InputField emailLoginField;
    public TMP_InputField passwordLoginField;
    public TMP_Text warningLoginText;
    public TMP_Text confirmLoginText;

    //Register variables
    [Header("Register")]
    public TMP_InputField usernameRegisterField;
    public TMP_InputField emailRegisterField;
    public TMP_InputField passwordRegisterField;
    public TMP_InputField passwordRegisterVerifyField;
    public TMP_Text warningRegisterText;
    
    
    
    [SerializeField] private DatabaseSystem dataBase;
    [SerializeField] private TextMeshProUGUI anonNameInput;
    [SerializeField] private GameObject LoginPanel;


    private FirebaseAuth _auth;

    private void Awake()
    {
        _auth = FirebaseAuth.DefaultInstance;
    }

    private void Start()
    {
        CheckUserAuth();
    }

    public void CheckUserAuth()
    {
        if (_auth.CurrentUser != null)
        {
            Time.timeScale = 1;
            LoginPanel.SetActive(false);
            Debug.LogFormat("User signed in successfully!\n Name: {0}, ID: ({1})", _auth.CurrentUser.DisplayName, _auth.CurrentUser.UserId);
            dataBase.LoadFromCloud();
        }
        else
        {
            Time.timeScale = 0;
            LoginPanel.SetActive(true);
            Debug.LogFormat("User no signed in!");
        }
    }

    public void AuthAnon()
    {
        if (anonNameInput.text.Length < 3)
            return;

        _auth.SignInAnonymouslyAsync().ContinueWith((task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInAnonymouslyAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
                return;
            }
            if (task.IsCompleted)
            {
                FirebaseUser newUser = task.Result;
                newUser.UpdateUserProfileAsync(new UserProfile() { DisplayName = anonNameInput.text });
                dataBase.SaveToCloud();
            }
        }));

        CheckUserAuth();
    }

    public void Logout()
    {
        if (_auth.CurrentUser != null)
        {
            _auth.SignOut();
            Debug.Log("User logout");
            CheckUserAuth();
        }
    }
}
