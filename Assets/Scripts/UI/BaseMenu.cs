using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMenu : MonoBehaviour
{
    public MenuName Name;
    public virtual void SetState(bool state)
    {
    }
}