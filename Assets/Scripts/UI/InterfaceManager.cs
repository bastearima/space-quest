﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


    public enum MenuName
    {
        Main, Pause, Game, Settings, Result, Gameover, ArcanoidShop
    }
    public class InterfaceManager : MonoBehaviour
    {
        private static InterfaceManager _instance;


        [SerializeField] private UserData userData;
        
        [SerializeField] private Text resultScoreText;
        [SerializeField] private Text resutScoreText;

        [SerializeField] private Text healthCountText;
        [SerializeField] private Text coinsCountText;


        [SerializeField] private Text scoreText, livesText, recordText;
        
        [SerializeField] private BaseMenu[] menus;
        private void Awake(){
            if (_instance != null){
                Destroy(_instance.gameObject);
            }
            _instance = this;
        }
        
        public static void Toggle(MenuName name)
        {
            foreach (BaseMenu baseMenu in _instance.menus)
            {
                var state =baseMenu.Name == name;
                baseMenu.gameObject.SetActive(state);
                baseMenu.SetState(state);
            }
        }
        
        public static void TurnOn(MenuName name)
        {
            var baseMenu = _instance.menus.SingleOrDefault(m=>m.Name==name);
            if(baseMenu==null) return;
            baseMenu.gameObject.SetActive(true);
            baseMenu.SetState(true);
        }
        
        public static void TurnOff(MenuName name)
        {
            var baseMenu = _instance.menus.SingleOrDefault(m=>m.Name==name);
            if(baseMenu==null) return;
            baseMenu.SetState(false);
            baseMenu.gameObject.SetActive(false);
        }

        public static void SetScoreText(int gameScore, float gameSeconds)
        {
            _instance.scoreText.text = gameScore.ToString();
            //_instance.resultScoreText.text = "Score: "+gameScore.ToString();
            //_instance.resutScoreText.text = ((int)gameSeconds).ToString()+"sec";
        }

        public static void UpdatePlayerData()
        {
            _instance.healthCountText.text = _instance.userData.health.maxHealth.ToString();
            _instance.coinsCountText.text = _instance.userData.balance.countOfCoins.ToString();
        }

        public static void SetLives(int lives)
        {
            _instance.livesText.text = lives.ToString();
        }

        public static void SetRecord(int record)
        {
            _instance.recordText.text = $"Record: {record}";
        }
    }
