using UnityEngine;

[CreateAssetMenu(menuName = "Data/UserData")]
public class UserData : ScriptableObject
{
    public string userName;
    
    [System.Serializable]
    public struct Balance
    {
        public int countOfCoins;
        public int countOfGems;
    }
    public Balance balance;

    [System.Serializable]
    public struct Score
    {
        public int ratingScore;
    }
    public Score score;

    [System.Serializable]
    public struct Health
    {
        public int maxHealth;
        public int curHealth;
    }
    public Health health;
}