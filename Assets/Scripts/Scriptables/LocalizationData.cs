﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Data/LocalizationData")]
public class LocalizationData : SerializedScriptableObject
{
    public enum Language
    {
        Russian,
        English
    }
    public Language lang;
    public Dictionary<string, LocalizationItem> itemsDict = new Dictionary<string, LocalizationItem>();

    [System.Serializable]
    public class LocalizationItem
    {
        public Dictionary<Language, string> dict = new Dictionary<Language, string>()
        {
            {Language.Russian, ""},
            {Language.English, ""}
        };
    }
}
